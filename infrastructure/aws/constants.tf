provider "aws" {
  region = "${var.aws_region}"
  version = "~> 1.41"
}

provider "template" {
  version = "~> 1.0"
}

data "aws_availability_zones" "available" {}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

data "template_file" "monitoring-config" {
  template = "${file("templates/telegraf.conf")}"
}

variable "tracing_service" {
  default = "ec2-18-184-49-169.eu-central-1.compute.amazonaws.com"
}