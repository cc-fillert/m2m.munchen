#cloud-config
write_files:
-   encoding: b64
    path:   /etc/telegraf/telegraf.d/telegraf.conf
    content: "${monitoring_config}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - nodejs

runcmd:
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_tarball}" /var/lib/router/router.tgz
  - su -l -c "mkdir router && cd router && tar xzf /var/lib/router/router.tgz && LOBSTERS_DNS_NAME=http://${lobsters_lb} MODLOG_DNS_NAME=${modlog_dns_name} ZIPKIN_SERVICE_URL=http://ec2-18-184-49-169.eu-central-1.compute.amazonaws.com:9411 RAILS_OPENTRACER_ENABLED=yes node index.js" ubuntu